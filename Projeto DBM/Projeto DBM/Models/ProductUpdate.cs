﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_DBM.Models
{
    public class ProductUpdate
    {
        public string description { get; set; }

        public double price { get; set; }

        public int amount { get; set; }

        public string provider { get; set; }

    }
}
