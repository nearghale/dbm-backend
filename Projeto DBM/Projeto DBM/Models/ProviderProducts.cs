﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_DBM.Models
{
    public class ProviderProducts
    {
        public string name { get; set; }

        public List<Entities.Product> products { get; set; }

    }
}
