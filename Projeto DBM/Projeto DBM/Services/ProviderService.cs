﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_DBM.Services
{
    public class ProviderService
    {
        private Repositories.MongoDB.PersistentRepository<Entities.Provider> _providerRepository;
        private Repositories.MongoDB.PersistentRepository<Entities.Product> _productRepository;

        public ProviderService(Repositories.MongoDB.PersistentRepository<Entities.Provider> providerRepository, 
                               Repositories.MongoDB.PersistentRepository<Entities.Product> productRepository)
        {

            _providerRepository = providerRepository;
            _productRepository = productRepository;


        }

        public Entities.Provider Create(Entities.Provider provider)
        {
            var newProvider = new Entities.Provider();

            newProvider.Name = provider.Name;

            return _providerRepository.Create(newProvider);
        }

        public void Update(Entities.Provider provider, Models.ProviderUpdate providerUpdate)
        {

            provider.Name = providerUpdate.name;

            _providerRepository.Update(provider.id, provider);

        }

        public void Delete(Entities.Provider provider)
        {
            _providerRepository.Remove(provider);

        }

         public Models.ProviderProducts GetProvider(string id)
        {
            var providerProducts = new Models.ProviderProducts();
            var provider = _providerRepository.FirstOrDefault(p => p.id == id);
            var products = _productRepository.Find(p => p.Provider == provider.Name);

            providerProducts.name = provider.Name;
            providerProducts.products = products;

            return providerProducts;
        }

        public List<Entities.Provider> GetAllProviders()
        {
            return _providerRepository.Find(p => true);
        }

    }
}
