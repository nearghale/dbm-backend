﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_DBM.Services
{
    public class ProductService
    {
        private Repositories.MongoDB.PersistentRepository<Entities.Product> _productRepository;

        public ProductService(Repositories.MongoDB.PersistentRepository<Entities.Product> productRepository)
        {

            _productRepository = productRepository;


        }

        public Entities.Product Create(Entities.Product account)
        {
            var newProduct = new Entities.Product();

            newProduct.Amount = account.Amount;
            newProduct.Description = account.Description;
            newProduct.Price = account.Price;
            newProduct.Provider = account.Provider;

            return _productRepository.Create(newProduct);

        }

        public void Update(Entities.Product product, Models.ProductUpdate productUpdate)
        {

            product.Amount = productUpdate.amount;
            product.Description = productUpdate.description;
            product.Price = productUpdate.price;
            product.Provider = productUpdate.provider;
            

            _productRepository.Update(product.id, product);

        }

        public List<Entities.Product> GetAllProducts()
        {
            return _productRepository.Find(p => true);
        }

        public Entities.Product GetProduct(string id)
        {
            return _productRepository.FirstOrDefault(p => p.id == id);
        }

        public void Delete(Entities.Product product)
        {
            _productRepository.Remove(product);

        }
    }
}
