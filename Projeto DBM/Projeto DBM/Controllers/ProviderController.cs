﻿using Microsoft.AspNetCore.Mvc;
using Projeto_DBM.Models.Configurations.MongoDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projeto_DBM.Services;


namespace Projeto_DBM.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProviderController : Controller
    {

        protected Repositories.MongoDB.PersistentRepository<Entities.Provider> providerRepository;
        protected Repositories.MongoDB.PersistentRepository<Entities.Product> productRepository;

        protected ProviderService providerService;

        public ProviderController(DataBaseSettings databaseSettings)
        {
            providerRepository = new Repositories.MongoDB.PersistentRepository<Entities.Provider>(databaseSettings, "provider");
            productRepository = new Repositories.MongoDB.PersistentRepository<Entities.Product>(databaseSettings, "product");
            providerService = new ProviderService(providerRepository, productRepository);

        }

        [HttpPost]
        public ActionResult<Entities.Provider> Create(Entities.Provider provider)
        {
       
            if (!(provider.Name.Length >= 10 & provider.Name.Length <= 50))
                return Unauthorized("INVALID_NAME_SIZE");

         
            if (provider.Name == "")
                return Unauthorized("NAME_REQUIRED");


            return providerService.Create(provider);
        }


        [HttpGet("{id}")]
        public ActionResult<Models.ProviderProducts> GetProvider([FromRoute] string id)
        {
            var provider = providerRepository.FirstOrDefault(a => a.id == id);
            if (provider == null)
                return this.Unauthorized("PROVIDER_NOT_FOUND");


            return providerService.GetProvider(id);
        }

        [HttpGet]
        public ActionResult<List<Entities.Provider>> GetAllProviders()
        {
            return providerService.GetAllProviders();
        }


        [HttpPut("{id}")]
        public ActionResult Update([FromRoute] string id, Models.ProviderUpdate providerUpdate)
        {

            var provider = providerRepository.FirstOrDefault(a => a.id == id);
            if (provider == null)
                return this.Unauthorized("PROVIDER_NOT_FOUND");

            if (!(providerUpdate.name.Length >= 10 & providerUpdate.name.Length <= 50))
                return Unauthorized("INVALID_NAME_SIZE");

            if (providerUpdate.name == "")
                return Unauthorized("NAME_REQUIRED");

            providerService.Update(provider, providerUpdate);
            return Ok();

        }

        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] string id)
        {

            var provider = providerRepository.FirstOrDefault(a => a.id == id);
            if (provider == null)
                return this.Unauthorized("PROVIDER_NOT_FOUND");

            var productExist = productRepository.FirstOrDefault(p => p.Provider == provider.Name);
            if(productExist != null)
                return this.Unauthorized("PROVIDER_HAVE_PRODUCT_IN_STOCK");



            providerService.Delete(provider);
            return Ok();


        }


    }
}
