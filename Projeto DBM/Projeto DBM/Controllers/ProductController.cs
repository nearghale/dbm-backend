﻿using Microsoft.AspNetCore.Mvc;
using Projeto_DBM.Models.Configurations.MongoDB;
using Projeto_DBM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_DBM.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        protected Repositories.MongoDB.PersistentRepository<Entities.Product> productRepository;
        protected ProductService productService;

        public ProductController(DataBaseSettings databaseSettings)
        {
            productRepository = new Repositories.MongoDB.PersistentRepository<Entities.Product>(databaseSettings, "product");
            productService = new ProductService(productRepository);

        }

        [HttpPost]
        public ActionResult<Entities.Product> Create(Entities.Product product)
        {
            var productAlreadyExist = productRepository.FirstOrDefault(a => a.Description == product.Description);

            if (productAlreadyExist != null)
                return Unauthorized("PRODUCT_ALREADY_EXISTS");

            if (!(product.Description.Length >= 10 & product.Description.Length <= 300))
                return Unauthorized("INVALID_DESCRIPTION_SIZE");

            if (product.Price == 0)
                return Unauthorized("PRICE_REQUIRED");

            if (product.Amount == 0)
                return Unauthorized("AMOUNT_REQUIRED");

            if (product.Provider == "")
                return Unauthorized("PROVIDER_REQUIRED");



            return productService.Create(product);
        }

        [HttpGet("{id}")]
        public ActionResult<Entities.Product> GetProduct([FromRoute] string id)
        {
            var product = productRepository.FirstOrDefault(a => a.id == id);
            if (product == null)
                return this.Unauthorized("PRODUCT_NOT_FOUND");


            return productService.GetProduct(id);
        }

        [HttpGet]
        public ActionResult<List<Entities.Product>> GetAllProducts()
        {
            return productService.GetAllProducts();
        }


        [HttpPut("{id}")]
        public ActionResult Update([FromRoute] string id, Models.ProductUpdate productUpdate)
        {

            var product = productRepository.FirstOrDefault(a => a.id == id);
            if (product == null)
                return this.Unauthorized("PRODUCT_NOT_FOUND");

            if (!(productUpdate.description.Length >= 10 & product.Description.Length <= 300))
                return Unauthorized("INVALID_DESCRIPTION_SIZE");

            if (productUpdate.price == 0)
                return Unauthorized("PRICE_REQUIRED");

            if (productUpdate.amount == 0)
                return Unauthorized("AMOUNT_REQUIRED");

            if (productUpdate.provider == "")
                return Unauthorized("PROVIDER_REQUIRED");

            productService.Update(product, productUpdate);
            return Ok();

        }

        [HttpDelete("{id}")]
        public ActionResult Delete([FromRoute] string id)
        {
            
            var product = productRepository.FirstOrDefault(a => a.id == id);
            if (product == null)
                return this.Unauthorized("PRODUCT_NOT_FOUND");

            productService.Delete(product);
            return Ok();


        }
    }
}
