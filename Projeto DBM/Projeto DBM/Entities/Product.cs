﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_DBM.Entities
{
    public class Product : MongoDB.Base
    {

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("price")]
        public double Price { get; set; }

        [JsonProperty("amount")]
        public int Amount { get; set; }

        [JsonProperty("provider")]
        public string Provider { get; set; }

    }
}
