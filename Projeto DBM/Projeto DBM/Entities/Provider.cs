﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projeto_DBM.Entities
{
    public class Provider : MongoDB.Base
    {
        [JsonProperty("name")]
        public string Name { get; set; }

    }
}
